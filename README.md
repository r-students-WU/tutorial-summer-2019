# Repository for the VW-SozÖk-Seep R-Tutorial Summer 2019

## Dates & Location

Time: Always 16:00 to 18:00 (4pm to 6pm)

- Mon, Apr 29 2019
    - D2.0.392 Seminar room (30) 	

- Mon, May 6 2019
    - D4.0.039 Seminar room (30) 

- Mon, May 13 2019
    - D4.0.039 Seminar room (30) 


## Software downloads

***You don't have to install anything beforehands - We'll set up R at the beginning of the first session***

### R

- Debian/Ubuntu: `sudo apt install r-base`
    - also see `https://gitlab.com/r-students-WU/DotfilesAndOtherNonRcode/tree/master/ubuntu18_04`

- RHEL/CentOS/Fedora: `sudo yum install R`

- (open)SUSE: https://cran.r-project.org/bin/linux/suse/

- MacOS: https://cran.r-project.org/bin/macosx/

- Windows: https://cran.r-project.org/bin/windows/

### R-Studio

https://www.rstudio.com/products/rstudio/download/#download

### Git

https://git-scm.com/downloads

### Other Resources

https://imsmwu.github.io/MRDA2018/_book/index.html

