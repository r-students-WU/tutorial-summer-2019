set.seed(1234)
X <- matrix(rnorm(1500, 0, 50), ncol = 3)
X <- cbind(1, X)

N <- nrow(X)
M <- ncol(X)
true_beta <- as.matrix(c(52, 3, 7, -24), ncol = 1)
y <- X %*% true_beta + rnorm(N, 0, 50)

nu0 <- 50
s02 <- 1/10e-1

beta0 <- matrix(c(50, 0, 10, -20), ncol = 1)
V0 <- diag(c(20, 5, 20, 10))
V0inv <- diag(diag(1/V0))

XX <- crossprod(X)
XXinv <- solve(XX)
Xy <- crossprod(X, y)

betaOLS <- XXinv %*% Xy

epsOLS <- y - X %*% betaOLS
dfOLS <- N - M
s2OLS <- crossprod(epsOLS)/dfOLS

nu1 <- nu0 + N

Shape <- function(nu){
    return(0.5 * nu)
}

Rate <- function(nu, s12){
    return(Shape(nu) * s12)
}

Scale <- function(nu, s12){
    return(1/Rate(nu, s12))
}

d_burn <- 10000
d_save <- 100000
d_tot <- d_burn + d_save

beta_store <- matrix(NA, nrow = d_save, ncol = M)
h_store <- matrix(NA, nrow = d_save, ncol = 1)
y_pred_store <- matrix(NA, nrow = d_save, ncol = 1)

x_pred <- c(1, 50, 10, -200)

h_draw <- as.numeric(1/s2OLS)

start <- Sys.time()
for(iter in 1:d_tot){
    # 1. Draw beta conditional on h
    V1inv <- V0inv + h_draw * XX
    V1 <- solve(V1inv)
    beta1 <- V1 %*% (V0inv %*% beta0 + h_draw * Xy)
    beta_draw <- beta1 + t(chol(V1)) %*% rnorm(M)    
    # 2. Draw h conditional on beta
    eps <- y - X %*% beta_draw
    s12 <- (1/nu1) * (crossprod(eps) + nu0 * s02) 
    h_draw <- rgamma(1, shape = Shape(nu1), rate = Rate(nu1, s12)) 
    # 3. Discard or save 
    if (iter > d_burn){
        row <- iter-d_burn
        beta_store[row, ] <- beta_draw
        h_store[row, 1] <- h_draw 
        y_pred_store[row, 1] <- x_pred %*% beta_draw + rnorm(1, 0, sqrt(1/h_draw))
    }
}
end <- Sys.time()
end-start

plot(density(y_pred_store))
hist(y_pred_store)
plot(density(beta_store[,4]))
library(Rcpp)

hist(sqrt(h_store))

sourceCpp("gibbsOnly.cpp")

start <- Sys.time()
out <- iNG(X, y, V0inv, beta0, nu0, s02, h_draw, 100000, 10000, x_pred)
end <- Sys.time()
end-start
plot(density(out$Ypred))

hist(out$Ypred)

mean(out$h)
quantile(out$h, c(.025, .95))

library(rstan)
options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

standat <- list(N = length(y), M = ncol(X), 
                X = X, y = as.vector(y), 
                x_pred = x_pred,
                beta_0 = as.vector(beta0), V_0 = diag(V0), nu_0 = nu0, s_02 = s02)

start <- Sys.time()
model_stan <- stan("iNG.stan", data = standat, chains = 4, iter = 25000, seed = 1234, refresh = 2000)
Sys.time() - start

source("stan_utility.r")
check_all_diagnostics(model_stan)
plot(model_stan, plotfun = 'hist', pars = 'y_pred')

plot(model_stan, plotfun = 'hist', pars = 'h')

library(shinystan)
launch_shinystan(model_stan)
